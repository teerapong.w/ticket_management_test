var express = require('express');
var router = express.Router();
const fs = require('fs');
let jsonData = require('../ticket.json');


router.post('/ticket', async (req, res) => {
  try {
    let title = !req.body.title ? "" : req.body.title
    let description = !req.body.description ? "" : req.body.description
    let contactInformation = !req.body.contactInformation ? "" : req.body.contactInformation

    if (!req.body.title) throw new Error('title is Empty')
    if (!req.body.contactInformation) throw new Error('title is contactInformation')

    let dataCreate = {
      "id": jsonData.dataTicket.length + 1,
      "title": title,
      "description": description,
      "status": "pending",
      "contactInformation": contactInformation,
      "createdAt": new Date(),
      "updateAt": new Date(),
    }

    jsonData.dataTicket.push(dataCreate)
    fs.writeFileSync('./ticket.json', JSON.stringify(jsonData));

    res.status(200).send({
      'success': true,
      'result': "Create Ticket Success"
    })

  } catch (error) {
    console.log(error.message)
    return res.status(400).send({
      success: false,
      msg: error.message
    })
  }
})

router.put('/statusTicketById', async (req, res) => {
  try {
    let id = !req.body.id ? "" : req.body.id
    let status = !req.body.status ? "" : req.body.status
    let title = !req.body.title ? "" : req.body.title
    let description = !req.body.description ? "" : req.body.description
    let contactInformation = !req.body.contactInformation ? "" : req.body.contactInformation

    let dataArray = jsonData.dataTicket
    let dataNum = await dataArray.findIndex(a => a.id.toString() == id)
    if (dataNum < 0) throw new Error('Not found data ticket by id = ' + id)
    // update infomation
    if (title) { jsonData.dataTicket[dataNum].title = title }
    if (description) { jsonData.dataTicket[dataNum].description = description }
    if (contactInformation) { jsonData.dataTicket[dataNum].contactInformation = contactInformation }
    if (status) { jsonData.dataTicket[dataNum].status = status }

    jsonData.dataTicket[dataNum].updateAt = new Date()

    fs.writeFileSync('./ticket.json', JSON.stringify(jsonData));

    res.status(200).send({
      'success': true,
      'result': "Update data Ticket Success"
    })
  } catch (error) {
    console.log(error.message)
    return res.status(400).send({
      success: false,
      msg: error.message
    })
  }
})

router.get('/dataTicketAndFilter', async (req, res) => {
  try {
    let status = !req.query.status ? "" : req.query.status
    let dataArray = jsonData.dataTicket.sort(function (a, b) {
      return new Date(b.updateAt) - new Date(a.updateAt);
    });
    let result = []
    let data = dataArray
    // sort data by status 
    let pending = []
    let accepted = []
    let resolved = []
    let rejected = []
    for (let data of dataArray) {
      if (data.status == 'pending') { pending.push(data) }
      if (data.status == 'accepted') { accepted.push(data) }
      if (data.status == 'resolved') { resolved.push(data) }
      if (data.status == 'rejected') { rejected.push(data) }
    }
    result.push(...pending, ...accepted, ...resolved, ...rejected);

    // console.log(status);
    if (status) {
      data = await dataArray.filter(a => a.status == status)
    }


    return res.status(200).send({
      'success': true,
      'result': data
    })
  } catch (error) {
    console.log(error.message)
    return res.status(400).send({
      success: false,
      msg: error.message
    })
  }
})



module.exports = router;
